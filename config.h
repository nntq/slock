/* user and group to drop privileges to */
static const char *user  = "nobody";
static const char *group = "nobody";

static const char *colorname[NUMCOLS] = {
	[INIT] =   "#f1c40f",     /* after initialization */
	[INPUT] =  "#3498db",   /* during input */
	[FAILED] = "#e74c3c",   /* wrong password */
};

/* treat a cleared input like a wrong password (color) */
static const int failonclear = 0;
